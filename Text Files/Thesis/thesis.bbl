% $ biblatex auxiliary file $
% $ biblatex bbl format version 2.8 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\sortlist[entry]{nty/global/}
  \entry{beltramelli2017pix2code}{article}{}
    \name{author}{1}{}{%
      {{hash=BT}{%
         family={Beltramelli},
         familyi={B\bibinitperiod},
         given={Tony},
         giveni={T\bibinitperiod},
      }}%
    }
    \strng{namehash}{BT1}
    \strng{fullhash}{BT1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{B}
    \field{sortinithash}{B}
    \field{title}{pix2code: Generating Code from a Graphical User Interface
  Screenshot}
    \field{journaltitle}{arXiv preprint arXiv:1705.07962}
    \field{year}{2017}
  \endentry

  \entry{SGD}{article}{}
    \name{author}{1}{}{%
      {{hash=BL}{%
         family={Bottou},
         familyi={B\bibinitperiod},
         given={L´eon},
         giveni={L\bibinitperiod},
      }}%
    }
    \strng{namehash}{BL1}
    \strng{fullhash}{BL1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{B}
    \field{sortinithash}{B}
    \field{title}{Stochastic Gradient Descent Tricks}
    \verb{url}
    \verb https://cilvr.cs.nyu.edu/diglib/lsml/bottou-sgd-tricks-2012.pdf
    \endverb
    \field{journaltitle}{CILVR AT NYU}
    \field{year}{2013}
    \warn{\item Invalid format of field 'urldate'}
  \endentry

  \entry{over}{online}{}
    \name{author}{1}{}{%
      {{hash=BJ}{%
         family={Brownlee},
         familyi={B\bibinitperiod},
         given={Jason},
         giveni={J\bibinitperiod},
      }}%
    }
    \strng{namehash}{BJ1}
    \strng{fullhash}{BJ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{B}
    \field{sortinithash}{B}
    \field{title}{Overfitting and Underfitting With Machine Learning
  Algorithms}
    \verb{url}
    \verb https://machinelearningmastery.com/overfitting-and-underfitting-with-
    \verb machine-learning-algorithms/
    \endverb
    \field{year}{2016}
  \endentry

  \entry{batch}{online}{}
    \name{author}{1}{}{%
      {{hash=BJ}{%
         family={Brownlee},
         familyi={B\bibinitperiod},
         given={Jason},
         giveni={J\bibinitperiod},
      }}%
    }
    \strng{namehash}{BJ1}
    \strng{fullhash}{BJ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{B}
    \field{sortinithash}{B}
    \field{title}{What is the Difference Between a Batch and an Epoch in a
  Neural Network?}
    \verb{url}
    \verb https://machinelearningmastery.com/difference-between-a-batch-and-an-
    \verb epoch/
    \endverb
    \field{year}{2018}
  \endentry

  \entry{lin}{article}{}
    \name{author}{1}{}{%
      {{hash=CG}{%
         family={Cybenkot},
         familyi={C\bibinitperiod},
         given={G.},
         giveni={G\bibinitperiod},
      }}%
    }
    \strng{namehash}{CG1}
    \strng{fullhash}{CG1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{C}
    \field{sortinithash}{C}
    \field{title}{Approximation by Superpositions of a Sigmoidal Function}
    \verb{url}
    \verb https://pdfs.semanticscholar.org/05ce/b32839c26c8d2cb38d5529cf7720a68
    \verb c3fab.pdf
    \endverb
    \field{journaltitle}{Mathematics of Control,Signals, and Systems}
    \field{year}{1989}
  \endentry

  \entry{loss}{article}{}
    \name{author}{1}{}{%
      {{hash=GJ}{%
         family={Gage},
         familyi={G\bibinitperiod},
         given={Justin},
         giveni={J\bibinitperiod},
      }}%
    }
    \strng{namehash}{GJ1}
    \strng{fullhash}{GJ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{G}
    \field{sortinithash}{G}
    \field{title}{Introduction to Loss Functions}
    \verb{url}
    \verb https://blog.algorithmia.com/introduction-to-loss-functions/
    \endverb
    \field{journaltitle}{algorithmia blog}
    \field{year}{2014}
  \endentry

  \entry{Goodfellow-et-al-2016}{book}{}
    \name{author}{3}{}{%
      {{hash=GI}{%
         family={Goodfellow},
         familyi={G\bibinitperiod},
         given={Ian},
         giveni={I\bibinitperiod},
      }}%
      {{hash=BY}{%
         family={Bengio},
         familyi={B\bibinitperiod},
         given={Yoshua},
         giveni={Y\bibinitperiod},
      }}%
      {{hash=CA}{%
         family={Courville},
         familyi={C\bibinitperiod},
         given={Aaron},
         giveni={A\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {MIT Press}%
    }
    \strng{namehash}{GIBYCA1}
    \strng{fullhash}{GIBYCA1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{G}
    \field{sortinithash}{G}
    \field{note}{\url{http://www.deeplearningbook.org}}
    \field{title}{Deep Learning}
    \field{year}{2016}
  \endentry

  \entry{rms}{misc}{}
    \name{author}{1}{}{%
      {{hash=HG}{%
         family={Hinton},
         familyi={H\bibinitperiod},
         given={Geoffrey},
         giveni={G\bibinitperiod},
      }}%
    }
    \strng{namehash}{HG1}
    \strng{fullhash}{HG1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{H}
    \field{sortinithash}{H}
    \field{title}{Rmsprop: Divide the gradient by a running average of its
  recent magnitude}
    \verb{url}
    \verb https://www.coursera.org/lecture/neural-networks/rmsprop-divide-the-g
    \verb radient-by-a-running-average-of-its-recent-magnitude-YQHki
    \endverb
  \endentry

  \entry{Hopfield2554}{article}{}
    \name{author}{1}{}{%
      {{hash=HJJ}{%
         family={Hopfield},
         familyi={H\bibinitperiod},
         given={J\bibnamedelima J},
         giveni={J\bibinitperiod\bibinitdelim J\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {National Academy of Sciences}%
    }
    \strng{namehash}{HJJ1}
    \strng{fullhash}{HJJ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{H}
    \field{sortinithash}{H}
    \field{abstract}{%
    Computational properties of use of biological organisms or to the
  construction of computers can emerge as collective properties of systems
  having a large number of simple equivalent components (or neurons). The
  physical meaning of content-addressable memory is described by an appropriate
  phase space flow of the state of a system. A model of such a system is given,
  based on aspects of neurobiology but readily adapted to integrated circuits.
  The collective properties of this model produce a content-addressable memory
  which correctly yields an entire memory from any subpart of sufficient size.
  The algorithm for the time evolution of the state of the system is based on
  asynchronous parallel processing. Additional emergent collective properties
  include some capacity for generalization, familiarity recognition,
  categorization, error correction, and time sequence retention. The collective
  properties are only weakly sensitive to details of the modeling or the
  failure of individual devices.%
    }
    \verb{doi}
    \verb 10.1073/pnas.79.8.2554
    \endverb
    \verb{eprint}
    \verb http://www.pnas.org/content/79/8/2554.full.pdf
    \endverb
    \field{issn}{0027-8424}
    \field{number}{8}
    \field{pages}{2554\bibrangedash 2558}
    \field{title}{Neural networks and physical systems with emergent collective
  computational abilities}
    \verb{url}
    \verb http://www.pnas.org/content/79/8/2554
    \endverb
    \field{volume}{79}
    \field{journaltitle}{Proceedings of the National Academy of Sciences}
    \field{year}{1982}
  \endentry

  \entry{stat}{online}{}
    \field{labeltitlesource}{title}
    \field{sortinit}{I}
    \field{sortinithash}{I}
    \field{title}{Internet-Trends 2018. Statistiken \& Fakten aus den USA und
  weltweit}
    \verb{url}
    \verb https://de.vpnmentor.com/blog/internet-trends-statistiken-fakten-aus-
    \verb den-usa-und-weltweit/
    \endverb
    \field{year}{2018}
  \endentry

  \entry{WI}{article}{}
    \name{author}{1}{}{%
      {{hash=JYYTWC}{%
         family={Jim Y.F.\bibnamedelima Yam},
         familyi={J\bibinitperiod\bibinitdelim Y\bibinitperiod\bibinitdelim
  Y\bibinitperiod},
         given={Tommy W.S.\bibnamedelima Chow},
         giveni={T\bibinitperiod\bibinitdelim W\bibinitperiod\bibinitdelim
  C\bibinitperiod},
      }}%
    }
    \strng{namehash}{JYYTWC1}
    \strng{fullhash}{JYYTWC1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{J}
    \field{sortinithash}{J}
    \field{title}{A weight initialization method for improving training speed
  in feedforward neural network}
    \verb{url}
    \verb http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.470.8140&rep
    \verb =rep1&type=pdf
    \endverb
    \field{journaltitle}{Neurocomputing 30}
    \field{year}{1998}
  \endentry

  \entry{NIPS2012_4824}{incollection}{}
    \name{author}{3}{}{%
      {{hash=KA}{%
         family={Krizhevsky},
         familyi={K\bibinitperiod},
         given={Alex},
         giveni={A\bibinitperiod},
      }}%
      {{hash=SI}{%
         family={Sutskever},
         familyi={S\bibinitperiod},
         given={Ilya},
         giveni={I\bibinitperiod},
      }}%
      {{hash=HGE}{%
         family={Hinton},
         familyi={H\bibinitperiod},
         given={Geoffrey\bibnamedelima E},
         giveni={G\bibinitperiod\bibinitdelim E\bibinitperiod},
      }}%
    }
    \name{editor}{4}{}{%
      {{hash=PF}{%
         family={Pereira},
         familyi={P\bibinitperiod},
         given={F.},
         giveni={F\bibinitperiod},
      }}%
      {{hash=BCJC}{%
         family={Burges},
         familyi={B\bibinitperiod},
         given={C.\bibnamedelima J.\bibnamedelima C.},
         giveni={C\bibinitperiod\bibinitdelim J\bibinitperiod\bibinitdelim
  C\bibinitperiod},
      }}%
      {{hash=BL}{%
         family={Bottou},
         familyi={B\bibinitperiod},
         given={L.},
         giveni={L\bibinitperiod},
      }}%
      {{hash=WKQ}{%
         family={Weinberger},
         familyi={W\bibinitperiod},
         given={K.\bibnamedelima Q.},
         giveni={K\bibinitperiod\bibinitdelim Q\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Curran Associates, Inc.}%
    }
    \strng{namehash}{KASIHGE1}
    \strng{fullhash}{KASIHGE1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{K}
    \field{sortinithash}{K}
    \field{booktitle}{Advances in Neural Information Processing Systems 25}
    \field{pages}{1097\bibrangedash 1105}
    \field{title}{ImageNet Classification with Deep Convolutional Neural
  Networks}
    \verb{url}
    \verb http://papers.nips.cc/paper/4824-imagenet-classification-with-deep-co
    \verb nvolutional-neural-networks.pdf
    \endverb
    \field{year}{2012}
  \endentry

  \entry{cost}{article}{}
    \name{author}{1}{}{%
      {{hash=MC}{%
         family={McDonald},
         familyi={M\bibinitperiod},
         given={Conor},
         giveni={C\bibinitperiod},
      }}%
    }
    \strng{namehash}{MC1}
    \strng{fullhash}{MC1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{M}
    \field{sortinithash}{M}
    \field{title}{Machine learning fundamentals (I): Cost functions and
  gradient descent}
    \verb{url}
    \verb https://towardsdatascience.com/machine-learning-fundamentals-via-line
    \verb ar-regression-41a5d11f5220
    \endverb
    \field{journaltitle}{Towards Data Science}
    \field{year}{27.11.2017}
  \endentry

  \entry{rprop}{report}{}
    \name{author}{1}{}{%
      {{hash=RM}{%
         family={Riedmiller},
         familyi={R\bibinitperiod},
         given={Martin},
         giveni={M\bibinitperiod},
      }}%
    }
    \strng{namehash}{RM1}
    \strng{fullhash}{RM1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{R}
    \field{sortinithash}{R}
    \field{title}{Rprop - Description and Implementation Details}
    \verb{url}
    \verb http://www.inf.fu-berlin.de/lehre/WS06/Musterererkennung/Paper/rprop.
    \verb pdf
    \endverb
    \field{year}{1994}
  \endentry

  \entry{hold}{book}{}
    \name{author}{1}{}{%
      {{hash=RR}{%
         family={Rojas},
         familyi={R\bibinitperiod},
         given={R.},
         giveni={R\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Springer-Verlag}%
    }
    \strng{namehash}{RR1}
    \strng{fullhash}{RR1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{R}
    \field{sortinithash}{R}
    \field{isbn}{978-3-642-61068-4}
    \field{title}{Neural Networks}
    \field{year}{1996}
  \endentry

  \entry{dropout}{article}{}
    \name{author}{1}{}{%
      {{hash=SNSGHAKISR}{%
         family={Salakhutdinov},
         familyi={S\bibinitperiod},
         given={Nitish Srivastava Geoffrey Hinton Alex Krizhevsky Ilya
  Sutskever\bibnamedelima Ruslan},
         giveni={N\bibinitperiod\bibinitdelim S\bibinitperiod G\bibinitperiod
  H\bibinitperiod A\bibinitperiod K\bibinitperiod I\bibinitperiod
  S\bibinitperiod\bibinitdelim R\bibinitperiod},
      }}%
    }
    \strng{namehash}{SNSGHAKISR1}
    \strng{fullhash}{SNSGHAKISR1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{S}
    \field{sortinithash}{S}
    \field{title}{Dropout: A Simple Way to Prevent Neural Networks from
  Overfitting}
    \field{journaltitle}{Journal of Machine Learning Research}
    \field{year}{2014}
  \endentry

  \entry{PML}{book}{}
    \name{author}{1}{}{%
      {{hash=SRVM}{%
         family={Sebastian\bibnamedelima Raschka},
         familyi={S\bibinitperiod\bibinitdelim R\bibinitperiod},
         given={Vahid\bibnamedelima Mirjalili},
         giveni={V\bibinitperiod\bibinitdelim M\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {Packt Publishing Ltd.}%
    }
    \strng{namehash}{SRVM1}
    \strng{fullhash}{SRVM1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{S}
    \field{sortinithash}{S}
    \field{isbn}{978-1-78712-593-3}
    \field{title}{{Python Machine Learning: Machine Learning and Deep Learning
  with Python, scikit-kearn, and TensorFlow, 2nd Edition, E}}
    \list{location}{1}{%
      {Birmingham, UK}%
    }
    \field{year}{2017}
  \endentry

  \entry{Percep}{book}{}
    \name{author}{1}{}{%
      {{hash=uSAPMLM}{%
         prefix={und},
         prefixi={u\bibinitperiod},
         family={Seymour A.\bibnamedelima Papert},
         familyi={S\bibinitperiod\bibinitdelim A\bibinitperiod\bibinitdelim
  P\bibinitperiod},
         given={Marvin L.\bibnamedelima Minsky},
         giveni={M\bibinitperiod\bibinitdelim L\bibinitperiod\bibinitdelim
  M\bibinitperiod},
      }}%
    }
    \strng{namehash}{SAPMLMu1}
    \strng{fullhash}{SAPMLMu1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{S}
    \field{sortinithash}{S}
    \field{isbn}{0-262-63111-3}
    \field{title}{{Perceptrons - Expanded Edition}}
    \field{year}{1988}
  \endentry

  \entry{2014arXiv1409.1556S}{article}{}
    \name{author}{2}{}{%
      {{hash=SK}{%
         family={{Simonyan}},
         familyi={S\bibinitperiod},
         given={K.},
         giveni={K\bibinitperiod},
      }}%
      {{hash=ZA}{%
         family={{Zisserman}},
         familyi={Z\bibinitperiod},
         given={A.},
         giveni={A\bibinitperiod},
      }}%
    }
    \keyw{Computer Science - Computer Vision and Pattern Recognition}
    \strng{namehash}{SKZA1}
    \strng{fullhash}{SKZA1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{S}
    \field{sortinithash}{S}
    \verb{eprint}
    \verb 1409.1556
    \endverb
    \field{title}{{Very Deep Convolutional Networks for Large-Scale Image
  Recognition}}
    \field{journaltitle}{ArXiv e-prints}
    \field{eprinttype}{arXiv}
    \field{eprintclass}{cs.CV}
    \field{month}{09}
    \field{year}{2014}
  \endentry

  \entry{MSE}{article}{}
    \name{author}{1}{}{%
      {{hash=ZM}{%
         family={Zavershynskyi},
         familyi={Z\bibinitperiod},
         given={Maksym},
         giveni={M\bibinitperiod},
      }}%
    }
    \strng{namehash}{ZM1}
    \strng{fullhash}{ZM1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{Z}
    \field{sortinithash}{Z}
    \field{title}{MSE and Bias-Variance decomposition}
    \verb{url}
    \verb https://towardsdatascience.com/mse-and-bias-variance-decomposition-77
    \verb 449dd2ff55
    \endverb
    \field{journaltitle}{Towards Data Science}
    \field{year}{2017}
  \endentry

  \entry{DBLP:journals/corr/abs-1212-5701}{article}{}
    \name{author}{1}{}{%
      {{hash=ZMD}{%
         family={Zeiler},
         familyi={Z\bibinitperiod},
         given={Matthew\bibnamedelima D.},
         giveni={M\bibinitperiod\bibinitdelim D\bibinitperiod},
      }}%
    }
    \strng{namehash}{ZMD1}
    \strng{fullhash}{ZMD1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{Z}
    \field{sortinithash}{Z}
    \verb{eprint}
    \verb 1212.5701
    \endverb
    \field{title}{{ADADELTA:} An Adaptive Learning Rate Method}
    \verb{url}
    \verb http://arxiv.org/abs/1212.5701
    \endverb
    \field{volume}{abs/1212.5701}
    \field{journaltitle}{CoRR}
    \field{eprinttype}{arXiv}
    \field{year}{2012}
  \endentry
\endsortlist
\endinput
