\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\numberline {I}Einleitung}{1}{part.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{2}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Zielsetzung}{2}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Aufbau der Arbeit}{3}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\numberline {II}Grundlagen}{4}{part.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Anforderungen}{5}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Anforderungen an das System}{5}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Code von Nutzerschnittstellenelementen auf Webseiten}{5}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}Arten von Nutzerschnittstellenelementen}{6}{subsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Knopf-/ Klick-Elemente}{6}{section*.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Freitexteingabefelder}{6}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Bin\IeC {\"a}re Eingabefelder}{7}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Kontrollbox}{7}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Radiobutton}{7}{section*.12}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Dropdown-Liste}{7}{section*.14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Datumswahl-Element}{8}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Design der Nutzerschnittstellen}{8}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Zusammenfassung von Nutzerschnittstellenelementen}{8}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\numberline {III}Technik}{9}{part.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Technische Komponenten}{10}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}L\IeC {\"o}sungsansatz}{10}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Maschinelles Lernen}{10}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Lernvorgang}{11}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline \IeC {\"U}berwachtes Lernen}{11}{section*.18}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Un\IeC {\"u}berwachtes Lernen}{11}{section*.19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}Metriken}{12}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Mittlerer absoluter Fehler}{12}{section*.20}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Mittlere quadratische Abweichung}{12}{section*.21}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Genauigkeit}{12}{section*.22}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Verlust}{13}{section*.23}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.3}Gradienten}{13}{subsection.3.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.4}Hyperparameter}{14}{subsection.3.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.5}Problematiken}{14}{subsection.3.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline \IeC {\"U}beranpassung}{14}{section*.25}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Unteranpassung}{14}{section*.26}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Neuronale Netze/Deep Learning}{15}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Neuronale Netze}{15}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Gradienten}{15}{section*.27}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Perzeptron}{15}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Einlagiges Perzeptron}{16}{section*.29}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Mehrlagiges Perzeptron}{16}{section*.30}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Bilderkennung mit maschinellem Lernen}{17}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.4}Maschinelles Sehen}{17}{subsection.3.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.5}Deep Learning}{18}{subsection.3.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Convoluted Neural Networks}{18}{section*.33}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Faltungs-schichten}{18}{section*.35}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Pooling-Schichten}{19}{section*.37}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.6}Faltungseinheit}{20}{subsection.3.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Vollverbundene Schichten}{20}{section*.39}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.7}Hyperparameter in neuronalen Netzen}{20}{subsection.3.3.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Hyperparameter im Training}{20}{section*.40}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Lernrate}{21}{section*.41}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Anzahl der Iterationen}{21}{section*.42}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Stapelgr\IeC {\"o}\IeC {\ss }e}{21}{section*.43}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Hyperparameter in der Netzarchitektur}{21}{section*.44}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Aktivierungsfunktion}{21}{section*.45}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Rectified linear unit}{22}{section*.46}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Sigmoid}{22}{section*.47}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Anzahl der versteckten Schichten}{22}{section*.48}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Ausfall/Dropout}{22}{section*.49}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Initialgewichte des Netzwerks}{22}{section*.50}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Optimierungsalgorithmus}{23}{section*.51}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Stochastischer Gradientenabstieg}{23}{section*.52}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline RMSProp}{23}{section*.53}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{\nonumberline Adadelta}{23}{section*.54}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Vorhandene Systeme}{24}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Pix2Code}{24}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Funktionsweise}{24}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Probleme mit Pix2Code}{24}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Fehlen einer optischen Zeichenerkennung}{25}{section*.55}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Eingeschr\IeC {\"a}nkte Sprachunterst\IeC {\"u}tzung}{25}{section*.56}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Keine Klassifizierung m\IeC {\"o}glich}{25}{section*.57}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Nur M\IeC {\"o}glichkeit f\IeC {\"u}r Auszeichnungssprachcode Generierung}{25}{section*.58}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Nutzbare Teile}{26}{section*.59}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\numberline {IV}Eigene Entwicklung}{27}{part.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Entwicklung eines eigenen neuronalen Netzes}{28}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Testdaten}{28}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Testdaten Generator}{28}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Probleme mit synthetischen Daten}{29}{subsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Implementierung}{29}{subsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Verwendete Technologien f\IeC {\"u}r das neuronale Netz}{30}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Python}{30}{section*.61}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Tensorflow}{31}{section*.62}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Keras}{31}{section*.63}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Struktur des Systems}{31}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.1}Vorverarbeitung}{32}{subsection.5.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.2}Computer Vision}{32}{subsection.5.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Klassifizierung}{32}{section*.65}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Optische Zeichenerkennung}{33}{section*.66}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.3}Semantische Analyse des eingegebenen Codes}{33}{subsection.5.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.4}Zusammenf\IeC {\"u}gen aller Teile}{33}{subsection.5.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4.5}Codegeneration}{34}{subsection.5.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Formulare f\IeC {\"u}r den Test}{34}{section*.67}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5}Implementierung des CNN}{34}{section.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.5.1}Hyperparameter}{37}{subsection.5.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.6}Erste Anwendung des Netzes}{37}{section.5.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.6.1}Hyperparameter Tuning}{38}{subsection.5.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline SGD}{38}{section*.69}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline RMSprop}{39}{section*.73}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Adadelta}{40}{section*.75}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Test mit realen Daten}{40}{section*.77}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.7}Codegenerierung}{41}{section.5.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.8}Ergebnisse}{41}{section.5.8}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\numberline {V}Fazit}{42}{part.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Fazit}{43}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Fehlende Teile der Arbeit}{43}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Optische Zeichenerkennung}{43}{section*.78}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Codegenerierung}{43}{section*.79}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Der n\IeC {\"a}chste Schritt}{44}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}Zukunftsausblick}{44}{section.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {part}{\numberline {VI}Appendix}{45}{part.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Zus\IeC {\"a}tzliche Listings}{46}{chapter*.80}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Netzstruktur}{46}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Verzeichnisse}{48}{chapter*.81}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Listings}{49}{chapter*.82}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Literatur}{51}{chapter*.83}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Eidesstattliche Erkl\IeC {\"a}rung}{52}{chapter*.84}
