@ARTICLE{2014arXiv1409.1556S,
   author = {{Simonyan}, K. and {Zisserman}, A.},
    title = "{Very Deep Convolutional Networks for Large-Scale Image Recognition}",
  journal = {ArXiv e-prints},
archivePrefix = "arXiv",
   eprint = {1409.1556},
 primaryClass = "cs.CV",
 keywords = {Computer Science - Computer Vision and Pattern Recognition},
     year = 2014,
    month = sep,
   adsurl = {http://adsabs.harvard.edu/abs/2014arXiv1409.1556S},
  adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
