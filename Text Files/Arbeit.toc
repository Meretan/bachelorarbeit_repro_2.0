\select@language {english}
\contentsline {part}{I\hspace {1em}Einleitug}{2}
\contentsline {chapter}{\numberline {1}Problemstellung}{3}
\contentsline {chapter}{\numberline {2}Anforderungen an das System}{4}
\contentsline {section}{\numberline {2.1}Pix2Code}{4}
\contentsline {subsection}{\numberline {2.1.1}Probleme mit Pix2Code}{4}
\contentsline {subsubsection}{Fehlen einer Ocr Pipeline}{4}
\contentsline {subsubsection}{Keine passende Sprachunterst\IeC {\"u}tzung}{5}
\contentsline {subsubsection}{Nutzbare Teile}{5}
\contentsline {section}{\numberline {2.2}Alexnet}{5}
\contentsline {chapter}{\numberline {3}Neurale Netze/Deep Learning}{6}
\contentsline {subsection}{\numberline {3.0.1}Bilderkennung mit neuralen Netzten}{6}
\contentsline {subsubsection}{Wie "sieht" ein Computer ein Bild}{6}
\contentsline {subsubsection}{Convoluted Neural Networks}{6}
\contentsline {subsubsection}{Convolutional Layer}{6}
\contentsline {subsubsection}{Pooling Layers}{6}
\contentsline {subsubsection}{fully connected Layers}{6}
\contentsline {chapter}{\numberline {4}Pix2Code}{7}
\contentsline {chapter}{\numberline {5}Anwendung des Projektes}{8}
\contentsline {chapter}{\numberline {6}Ergebniss der Anwendung}{9}
\contentsline {subsection}{\numberline {6.0.1}Probleme mit Pix2Code}{9}
\contentsline {chapter}{\numberline {7}Fazit}{10}
