!!!Use an IDE for initial work with this Project!!!

start all files using the run commands of the IDE

To start the process of generating the DataSet, open start_generators.py.
Set the desired number of entrys for the DataSet.
!!warning!! there is a bug in the webdriver where a proccess spawned by a thread cannot be closed. The treads will build up, dont use multiple times without restarting
The entrys are saved in Model Files/HTML/DataSet/Address and Model Files/HTML/DataSet/Names in the corresponding Folders for HTML, CSS, and the Images

If you want to use this DataSets, copy the Images into the Folders Model Files/TestData/train and Model Files/TestData/validation.
Split the Data up in a roughly 60/40 fashion.

To use the Neural Net, use the main.py method

Place images you want to get predicted after traning in the test folder. This requires tweaking the predict method in main.py

For training the model with the datasets, remove the '#' infront of 'fit' in line 128.
If you want to get the histograms for the training cycle, remove the '#' in lines 130-131.

If you want to predict the Images in the Model Files/TestData/test folder after the system has learned, use the predict() method in line 140
Remove the '#' infront
