#multi threading worker class

class Work:
    def work(self, number):
        from bs4 import BeautifulSoup
        from selenium import webdriver
        from selenium.webdriver.chrome.options import Options
        from os import path
        from Generator_Files import Randomizer
        from Generator_Files import UI_Address_Class
        from Generator_Files import Ui_Objects
        number = yield(number)
        UI_Adress_Class = UI_Address_Class.Ui_Address
        Ui_Objects = Ui_Objects.Ui_Ojects()
        Randomizer = Randomizer.Randomize()
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('window-size=1980x1024')
        driver = webdriver.Chrome(chrome_options=chrome_options)
        path = path
        string = Ui_Objects.get_ui_css_template()
        Ui_Objects.shuffle()
        string_number = str(number)
        filename = "Model Files//HTML//DataSet//CSS//Address_css" + string_number + ".css"
        file = open(filename, "w")
        file.write(string)
        file.close()
        j = Randomizer.random_number(0, 2, 1)
        print(j)
        if j == 0:
            print("a")
            file = open('Model Files//HTML//DataSet//Adress_blueprint.html', 'rb')
        else:
            print("b")
            file = open('Model Files//HTML//DataSet//Adress_blueprint_rows_street.html', 'rb')
        html = BeautifulSoup(file, 'html.parser')
        file.close()
        html = str(html)
        css = "..//CSS//Address_css" + string_number + ".css"
        html = html.replace('Address_css.css', css)
        html = html.replace('col-6', 'col-' + str(Randomizer.random_number(5, 8, 1)), 1)  #
        html = html.replace('cadetblue', '#' + str(Randomizer.random_color()))
        html = html.replace("5", str(Randomizer.random_number(0, 5, 1)), 1)
        html_name = "Model Files//HTML//DataSet//HTML//test" + string_number + ".html"
        text_file = open(html_name, "w")
        text_file.write(html)
        text_file.close()
    # get the name into the right format
        path = path
        file_path = "C://Users//Wessnit//Desktop//Bachelorarbeit//Working Files//Generator Files//" + html_name
        print(file_path)
        file_path = path.abspath(file_path)
        driver.get(file_path)
        if j == 0:
            save_name = 'Model Files//HTML//DataSet//IMG//Address//' + str(number) + '.png'
        else:
            save_name = 'Model Files//HTML//DataSet//IMG//Address_2_Rows//' + str(number) + '.png'
        driver.save_screenshot(save_name)
        number += 1
        print("done" + string_number)

    def work2(self, number):
        print("sss")
        number = number
        for idx, number in enumerate(number):
            from bs4 import BeautifulSoup
            from selenium import webdriver
            from selenium.webdriver.chrome.options import Options
            from os import path
            from Generator_Files import Randomizer
            from Generator_Files import UI_Address_Class
            from Generator_Files import Ui_Objects
            UI_Adress_Class = UI_Address_Class.Ui_Address
            Ui_Objects = Ui_Objects.Ui_Ojects()
            Randomizer = Randomizer.Randomize()
            chrome_options = Options()
            chrome_options.add_argument("--headless")
            chrome_options.add_argument('window-size=1980x1024')
            driver = webdriver.Chrome(chrome_options=chrome_options)
            path = path
            string = Ui_Objects.get_ui_css_template()
            Ui_Objects.shuffle()
            string_number = str(number)
            filename = "Model Files//HTML//DataSet//CSS//Address_css" + string_number + ".css"
            file = open(filename, "w")
            file.write(string)
            file.close()
            j = Randomizer.random_number(0, 2, 1)
            print(j)
            if j == 0:
                print("a")
                file = open('Model Files//HTML//DataSet//Adress_blueprint.html', 'rb')
            else:
                print("b")
                file = open('Model Files//HTML//DataSet//Adress_blueprint_rows_street.html', 'rb')
            html = BeautifulSoup(file, 'html.parser')
            file.close()
            html = str(html)
            css = "..//CSS//Address_css" + string_number + ".css"
            html = html.replace('Address_css.css', css)
            html = html.replace('col-6', 'col-' + str(Randomizer.random_number(5, 8, 1)), 1)  #
            html = html.replace('cadetblue', '#' + str(Randomizer.random_color()))
            html = html.replace("5", str(Randomizer.random_number(0, 5, 1)), 1)
            html_name = "Model Files//HTML//DataSet//HTML//test" + string_number + ".html"
            text_file = open(html_name, "w")
            text_file.write(html)
            text_file.close()
            # get the name into the right format
            path = path
            file_path = "C://Users//Wessnit//Desktop//Bachelorarbeit//Working Files//Generator Files//" + html_name
            print(file_path)
            file_path = path.abspath(file_path)
            driver.get(file_path)
            save_name = 'Model Files//HTML//DataSet//IMG//HTML' + str(number) + '.png'
            driver.save_screenshot(save_name)
            number += 1
            print("done" + string_number)