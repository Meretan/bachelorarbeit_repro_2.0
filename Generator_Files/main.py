from keras.preprocessing.image import ImageDataGenerator
from os import path
from Generator_Files.Models import Model_MK1
from Generator_Files.Models import Model_MK2
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras import optimizers
from PIL import Image
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import cv2
from keras.callbacks import *
from Generator_Files.Code_Generator import Gui_Code
from keras import metrics

# main method for machine learning
#important method calls are at the bottom

path = path.abspath('Model Files//TestData')
#build data generators
train_datagen = ImageDataGenerator(
        rescale=1./255,
)
test_datagen = ImageDataGenerator(rescale=1./255)
#set the model (Model_MK1 or Model_MK2) from model folder
model = Model_MK2.returnModel()
#get the optimisers
RMS = optimizers.RMSprop(lr=0.001)
SGD = optimizers.sgd(0.001, 0, 0, False)
Adadelta = optimizers.Adadelta(1.0)
#compile the model and set hyperparameters and metrics
model.compile(loss='mean_squared_error',
              optimizer=RMS,
              metrics=['accuracy', metrics.mae, metrics.mse])
#set the batch size
batch_size = 32
#get the model summary
model.summary()


#generator for train set
train_generator = train_datagen.flow_from_directory(
    'Model Files//TestData//train',
    target_size=(224, 224),
    batch_size=batch_size,
    class_mode='binary',
    color_mode='grayscale'
)

#generator for test set
validation_generator = test_datagen.flow_from_directory(
    'Model Files//TestData//validation',
    target_size=(224, 224),
    batch_size=batch_size,
    class_mode='binary',
    color_mode='grayscale'
)

#build histogramms for accuracy and loss
def plot(history):
    print(history.history.keys())
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()


#main method for training
#adjust hyperparameters here
def fit():
    history = model.fit_generator(
        train_generator,
        steps_per_epoch=30,
        epochs=5,
        validation_data=validation_generator,
        validation_steps=800 // batch_size,
        callbacks=[])
    model.save_weights('first_try.h5')
    return history

#depricated method
def sample(number):
    img = load_img('TestData//train//Address//Address0.png')  # this is a PIL image
    x = img_to_array(img)  # this is a Numpy array with shape (3, 150, 150)
    x = x.reshape((1,) + x.shape)  # this is a Numpy array with shape (1, 3, 150, 150)

    # the .flow() command below generates batches of randomly transformed images
    # and saves the results to the `preview/` directory
    i = 0
    for batch in train_datagen.flow(x, batch_size=1,
                                    save_to_dir='TestData//preview', save_prefix='Address', save_format='png'):
        i += 1
        if i > number:
            break

#main method for predicting
def predict():
    data = getarrayforprediction()
    model.load_weights('first_try.h5')
    prediction = model.predict_on_batch(data)
    print(prediction)
    return prediction


#mehtod for building a set from images, which the system can use
def getarrayforprediction():
    i = 0
    array = []
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    while i < 2: #change this number to the amount of pictures in the test folder. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        img = cv2.imread('Model Files//TestData//test//test' + str(i) + '.png', 1)
        img = cv2.resize(img, (224, 224))
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = np.reshape(img, [224, 224, 1])
        array.append(img)
        i = i+1
    array = np.array(array)
    return array


#enable the wanted methods here

#fit the system to the dataset
#fit()

#fit the model and get histograms for accuracy and loss
#history = fit()
#plot(history)


#predict images in the test folder
predict()