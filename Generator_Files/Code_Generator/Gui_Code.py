from bs4 import BeautifulSoup

#Gui code generator

class GuiCodeGenerator:

    Address = 'Address'
    Name = 'Name'
    Invalid = 'Invalid'

#get type of gui from prediction
    def gettypeofgui(self, prediction):

        if prediction == 0:
            return self.Address
        elif prediction == 1:
            return self.Name
        else:
            return prediction

#get gui from gui type
    def getguifromtype(self, guitype):
        if guitype == self.Address:
            elements = [["text", "name", "Name:"],["text", "street", "Street:"],["text", "number", "Number:"], ["text", "zipcode", "Zipcode:"], ["text", "city", "City:"], ["button", "submit", "submit:" ]]
        elif guitype == self.Name:
            elements = [["text", "name", "Name:"],["text", "surname", "Last Name:"],["text", "user", "User:"], ["text", "email", "Email:"], ["text", "passwort", "Passwort:"], ["text", "confirm", "Confirm passwort:"],["button", "submit", "submit:" ]]
        color = "#5F9EA0"
        string = self.getguicode(elements, color)
        return string

#get gui code
    def getguicode(self, elements, color):
        string = ""
        for member in elements:
            if member[0] == "text":
                part = self.gettextfieldsingle(member[1], member[2])
                string += part
            elif member[0] == "button":
                part = self.getbutton(member[1], member[2])
                string += part
        template = '<!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/html"><head><meta charset="utf-8"/><meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/><link href="bootstrap.css" rel="stylesheet"/><title>build</title></head><body><div class="container"><form class="container "><div class="container address_box" style="background-color:???">!!!</div></form></div></body></html>'
        template = str.replace(template, "!!!", string)
        template = str.replace(template, "???", color)
        return template

#get a single row text field
    def gettextfieldsingle(self, name, type):
        string = '<div class="form-group"><label for="!!!">NAME: </label><input type="text" name="!!!" id="!!!" class="form-control"></div>'
        string = str.replace(string, "!!!", type)
        string = str.replace(string, "NAME", name)
        return string

#get a double text field
    def gettextfielddouble(self, name, type):
        string = '<div class="form-group"><div class="row"><div class="col-6"><label for="!!!">NAME</label><input type="text" name="!!!" id="!!!" class="form-control"></div><div class="col-6"><label for="!!!">NAME</label><input type="text" name="!!!" id="!!!" class="form-control"></div></div></div>'
        string = str.replace(string, "!!!", type)
        string = str.replace(string, "NAME", name)
        return string

#get a button
    def getbutton(self, name, type):
        string = '<div class="form-group"><label for="submit">submit:</label><button name="submit" id="submit" class="form-control btn btn-primary">click</button> </div>'
        string = str.replace(string, "submit", type)
        string = str.replace(string, "click:", name)
        return string
