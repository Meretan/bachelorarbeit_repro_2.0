from Generator_Files import Randomizer
from bs4 import BeautifulSoup
# class for providing all needed components for Html generation


class Ui_Ojects:
    # initialise
    def __init__(self):
        randomizer = Randomizer.Randomize()
        self.number = randomizer.random_number(0, 5, 1)
        self.color = randomizer.random_color()
    # reinitialises color and number

    def shuffle(self):
        randomizer = Randomizer.Randomize()
        self.number = randomizer.random_number(0, 5, 1)
        self.color = randomizer.random_color()
    # get the css template

    def get_ui_css_template(self):
        template = ".address_box{padding:0rem;background: #" + str(self.color) + "}"
        return template
    # get the html template

    def get_ui_html_template(self, css):
        randomizer = Randomizer.Randomize()
        j = randomizer.random_number(0, 2, 1)
        if j == 0:
            file = open('Model Files//HTML//DataSet//Adress_blueprint.html', 'rb')
        else:
            file = open('Model Files//HTML//DataSet//Adress_blueprint_rows_street.html', 'rb')
        html = BeautifulSoup(file, 'html.parser')
        file.close()
        html = str(html)
        html = html.replace('Address_css.css', css)
        html = html.replace('col-6', 'col-' + str((5, 8, 1)), 1)  #
        return html

