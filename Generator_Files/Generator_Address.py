import time
from Generator_Files import work
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from os import path
from Generator_Files import Randomizer
from Generator_Files import UI_Address_Class
from Generator_Files import Ui_Objects
from multiprocessing import Pool
import os

#main method for generating the address dataset
#generates hmt, css and takes an image
#has to be called via start() method
#uses multithreading

# Initialise gloabl variables and Objects
UI_Address_Class = UI_Address_Class.Ui_Address
Ui_Objects = Ui_Objects.Ui_Ojects()
Randomizer = Randomizer.Randomize()
path = path
options = Options()
options.add_argument('--headless')
options.add_argument('--disable-extensions')
options.add_argument('window-size992x733')
driver = webdriver.Chrome(chrome_options=options)
processes = 12 # number of spawned child processes. use high numbers if the cpu allows it


#set to current directory bevore proceeding
pathtofile = "Change//this//to//your//path"

# Work proocess for Dataset generation
def worker(number):
    # Initialise needed variables for individual workers
    from bs4 import BeautifulSoup
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    from Generator_Files import Randomizer
    from Generator_Files import UI_Address_Class
    from Generator_Files import Ui_Objects
    Ui_Objects = Ui_Objects.Ui_Ojects()
    Randomizer = Randomizer.Randomize()
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument('window-size=1980x1400')
    driver = webdriver.Chrome(chrome_options=chrome_options)
    string = Ui_Objects.get_ui_css_template()
    Ui_Objects.shuffle()
    string_number = str(number)
    # get and save css
    filename = pathtofile + "//Model Files//HTML//DataSet//Address//CSS//Address_css" + string_number + ".css"
    file = open(filename, "w")
    file.write(string)
    file.close()
    # get and save html
    j = Randomizer.random_number(0, 2, 1)
    if j == 0:
        file = open('Model Files//HTML//DataSet//Adress_blueprint.html', 'rb')
    else:
        file = open('Model Files//HTML//DataSet//Adress_blueprint_rows_street.html', 'rb')
    html = BeautifulSoup(file, 'html.parser')
    file.close()
    html = str(html)
    css = "..//CSS//Address_css" + string_number + ".css"
    html = html.replace('Address_css.css', css)
    html = html.replace('col-6', 'col-' + str(Randomizer.random_number(5, 8, 1)), 1)  #
    html = html.replace('cadetblue', '#' + str(Randomizer.random_color()))
    html = html.replace("5", str(Randomizer.random_number(0, 5, 1)), 1)
    html_name = pathtofile + "//Model Files//HTML//DataSet//Address//HTML//test" + string_number + ".html"
    text_file = open(html_name, "w")
    text_file.write(html)
    text_file.close()
    # capture and save screenshot
    file_path = "Working Files//Generator Files//" + html_name
    driver.get(file_path)
    save_name = pathtofile + '//Model Files//HTML//DataSet//Address//IMG//Name' + str(number) + '.png'
    driver.save_screenshot(save_name)
    # increment counter
    number += 1
    if (number % 100) == 0:
        print("done" + str(int(string_number) + 1))


def start(number):
    # init the timer
    start_time = time.time()
    array = []
    i = 0
    # get the iter array
    if number < 2:
        number = 20
    while i <= number:
        array.append(i)
        i += 1
    x = iter(array)
    print("start process")
    # create worker pool
    p = Pool(processes=processes)
    # start worker pool
    p.map(worker, x)
    p.terminate()
    # tear down the drivers
    driver.close()
    driver.quit()
    print("--- %s seconds ---" % (time.time() - start_time))

if __name__ == '__main__':
    #init the timer
    start_time = time.time()
    work = work.Work()
    array = []
    i = 0
    #get the iter array
    while i <= 20:
        array.append(i)
        i += 1
    x = iter(array)
    print("start process")
    #create worker pool
    p = Pool(processes=processes)
    #start worker pool
    p.map(worker, x)
    p.terminate()
    #tear down the drivers
    driver.close()
    driver.quit()
    print("--- %s seconds ---" % (time.time() - start_time))