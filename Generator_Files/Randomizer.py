from random import randrange
from random import randint

#class for geting randomised intergers and hex colors

class Randomize:
    def random_number(self, low, high, step):
        return randrange(low, high, step)

    def random_color(self):
        color = "%06x" % randint(0, 0xFFFFFF)
        return color

